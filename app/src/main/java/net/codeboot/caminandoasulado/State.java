package net.codeboot.caminandoasulado;

import android.os.Parcel;
import android.os.Parcelable;

import com.wenchao.cardstack.CardStack;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class State implements Parcelable {

    byte[] mCard,mAdapter;

    public State(Parcel in){
        in.readByteArray(mAdapter);
        in.readByteArray(mCard);

        try {
            mCardAdapter=(SwipeImages) deserialize(mAdapter);
            mCardStack=(CardStack) deserialize(mCard);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    public CardStack getmCardStack() {
        return mCardStack;
    }

    public static byte[] serialize(Object obj) throws IOException {
        try(ByteArrayOutputStream b = new ByteArrayOutputStream()){
            try(ObjectOutputStream o = new ObjectOutputStream(b)){
                o.writeObject(obj);
            }
            return b.toByteArray();
        }
    }

    public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        try(ByteArrayInputStream b = new ByteArrayInputStream(bytes)){
            try(ObjectInputStream o = new ObjectInputStream(b)){
                return o.readObject();
            }
        }
    }

    private CardStack mCardStack;

    public SwipeImages getmCardAdapter() {
        return mCardAdapter;
    }

    private SwipeImages mCardAdapter;

    public State(CardStack mCardStack, SwipeImages mCardAdapter) {
        this.mCardAdapter=mCardAdapter;
        this.mCardStack=mCardStack;
    }

    public int describeContents() {
        return 0;
    }

    /** save object in parcel */
    public void writeToParcel(Parcel out, int flags) {
        try {
            out.writeByteArray(serialize(mCardAdapter));
            out.writeByteArray(serialize(mCardStack));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static final Parcelable.Creator<State> CREATOR
            = new Parcelable.Creator<State>() {
        public State createFromParcel(Parcel in) {
            return new State(in);
        }

        public State[] newArray(int size) {
            return new State[size];
        }
    };

}