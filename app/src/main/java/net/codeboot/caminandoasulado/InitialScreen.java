package net.codeboot.caminandoasulado;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.JsonReader;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import 	android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.wenchao.cardstack.CardStack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import static com.amazonaws.regions.ServiceAbbreviations.Email;

public class InitialScreen extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    static int index=0;
    // URL to get contacts JSON
    private ProgressDialog pDialog;
    private static String url = "http://perros.us-west-2.elasticbeanstalk.com/json";
    private static final String TAG = "Perros";

    static CardStack mCardStack;
    static SwipeImages mCardAdapter;
    Set<HashMap<String,String>> map= new HashSet<HashMap<String,String>>();
    private GestureDetectorCompat gestureDetector;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_initial_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState != null){
            State state = (State) savedInstanceState.getParcelable("state");
            mCardAdapter=state.getmCardAdapter();
            mCardStack=state.getmCardStack();
        }else if(mCardStack == null && mCardAdapter == null){

            mCardStack = (CardStack) findViewById(R.id.fragment);

            mCardStack.setContentResource(R.layout.swipe_images);
            mCardStack.setStackMargin(20);

            mCardAdapter = new SwipeImages(getApplicationContext(), 0);

            new GetPerros().execute();
        }else{
            mCardStack = (CardStack) findViewById(R.id.fragment);
            mCardStack.setContentResource(R.layout.swipe_images);
            mCardStack.setStackMargin(20);
            mCardStack.setAdapter(mCardAdapter);
            for (int i=0;i<index;i++){
                mCardStack.discardTop(0);
            }
        }

        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                getApplicationContext(),
                "us-west-2:a0c14c0b-b89c-4388-a4b0-f5031b8ae7dc", // Identity Pool ID
                Regions.US_WEST_2 // Region
        );

        AmazonS3 s3 = new AmazonS3Client(credentialsProvider);

        final View fragment = findViewById(R.id.fragment);
        fragment.setClickable(true);

        mCardStack.setListener(new CardStack.CardEventListener() {
            @Override
            public boolean swipeEnd(int i, float v) {
                return true;
            }

            @Override
            public boolean swipeStart(int i, float v) {
                return true;
            }

            @Override
            public boolean swipeContinue(int i, float v, float v1) {
                return true;
            }

            @Override
            public void discarded(int i, int i1) {
                if(i1 == 1 || i1 == 3){
                    Snackbar.make(fragment, "Perfecto! Ya envie un email", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                }
            }
            @Override
            public void topCardTapped() {
                startDetail();
            }
        });

        final View main = findViewById(R.id.mainView);
        ImageButton check= (ImageButton) (main.findViewById(R.id.check));
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCardStack.discardTop(1);
            }
        });

        ImageButton cross= (ImageButton) (main.findViewById(R.id.cross));
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCardStack.discardTop(2);
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        State state = (State) savedInstanceState.getParcelable("state");
        mCardAdapter=state.getmCardAdapter();
        mCardStack=state.getmCardStack();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        State state = new State(mCardStack,mCardAdapter);
        outState.putParcelable("state",state);
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetPerros extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(InitialScreen.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);

            if (jsonStr != null) {
                try {

                    byte[] bytes = jsonStr.getBytes(StandardCharsets.UTF_8);
                    ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
                    JsonReader reader = new JsonReader(new InputStreamReader(bais));
                    reader.beginArray();
                    while (reader.hasNext()) {
                        reader.beginObject();
                        HashMap <String,String> str= new HashMap<String,String>();
                        while (reader.hasNext()) {
                            String name = reader.nextName();
                            if (name.equals("foto")) {
                                reader.beginObject();
                                while (reader.hasNext()) {
                                    name = reader.nextName();
                                    str.put(name,reader.nextString());
                                }
                                reader.endObject();
                            } else {
                                try {
                                    str.put(name, reader.nextString());
                                }catch(RuntimeException a){
                                    reader.nextNull();
                                    str.put(name, "");
                                }
                            }
                        }
                        map.add(str);
                        reader.endObject();
                    }
                    reader.endArray();
                } catch (final IOException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });
                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */

            for (HashMap<String,String> s : map) {
                mCardAdapter.add(s);
            }
            mCardStack.setAdapter(mCardAdapter);
        }

    }

    public void startDetail(){
        Intent intent = new Intent(this, Info.class);
        index = mCardStack.getCurrIndex();
        HashMap<String, String> item = mCardAdapter.getItem(index);
        intent.putExtra("nombre", item.get("nombre"));
        intent.putExtra("actititud", item.get("actititud"));
        intent.putExtra("historia", item.get("historia"));
        intent.putExtra("url", item.get("url"));
        intent.putExtra("created_at", item.get("created_at"));
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.initial_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
