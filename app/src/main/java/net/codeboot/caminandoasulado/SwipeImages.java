package net.codeboot.caminandoasulado;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;

import static net.codeboot.caminandoasulado.R.id.imageView;

public class SwipeImages extends ArrayAdapter<HashMap<String,String>> {

    private static final String TAG = "Perros";

    public SwipeImages(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
    }

    @Override
    public View getView(int position, final View contentView, ViewGroup parent) {
        //supply the layout for your card
        TextView name = (TextView) (contentView.findViewById(R.id.name));
        TextView km = (TextView) (contentView.findViewById(R.id.KM));
        TextView descripcion = (TextView) (contentView.findViewById(R.id.descripcion));
        ImageView img= (ImageView)  (contentView.findViewById(R.id.perro));

        name.setText(getItem(position).get("nombre"));
        if(((String) getItem(position).get("created_at")).equals("2017-02-26T16:30:00.000Z")) {
            km.setText("5 km");
            descripcion.setText("Caminando a su Lado S.A. de C.V.");
        }else{
            km.setText("8 km");
            descripcion.setText("Señora Oli");
        }

        AnimationDrawable animPlaceholder = (AnimationDrawable)getContext().getDrawable(R.drawable.spin_animation);
        animPlaceholder.start(); // probably needed
        Glide.with(getContext())
                .load(getItem(position).get("url"))
                .placeholder(animPlaceholder)
                .into(img);

        return contentView;
    }
}