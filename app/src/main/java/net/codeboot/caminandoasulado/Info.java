package net.codeboot.caminandoasulado;

import android.graphics.drawable.AnimationDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class Info extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        TextView nombre = (TextView) (findViewById(R.id.name_value));
        nombre.setText(getIntent().getStringExtra("nombre"));
        TextView km = (TextView) (findViewById(R.id.KM_Value));
        if(((String) getIntent().getStringExtra("created_at")).equals("2017-02-26T16:30:00.000Z")) {
            km.setText("5 km");
            //descripcion.setText("Caminando a su Lado S.A. de C.V.");
        }else{
            km.setText("8 km");
            //descripcion.setText("Señora Oli");
        }
        TextView actitud = (TextView) (findViewById(R.id.actitud_value));
        actitud.setText(getIntent().getStringExtra("actititud"));
        TextView historia = (TextView) (findViewById(R.id.historia_value));
        historia.setText(getIntent().getStringExtra("historia"));

        ImageView view = (ImageView) findViewById(R.id.detail_foto);

        AnimationDrawable animPlaceholder = (AnimationDrawable)getBaseContext().getDrawable(R.drawable.spin_animation);
        animPlaceholder.start(); // probably needed
        Glide.with(getBaseContext())
                .load(getIntent().getStringExtra("url"))
                .placeholder(animPlaceholder)
                .into(view);
    }
}
